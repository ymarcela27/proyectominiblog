package com.mitocode.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.model.Persona;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named(value="usuarioBean")
@ViewScoped
public class UsuarioBean implements Serializable{
	
	@Inject
	private IUsuarioService usuarioService;
	private Usuario usuarioSeleccion;
	private Usuario usuario;
	private Usuario contrasenaNew;
	private String Password1;
	private String aux;
	private List<Usuario> lista;
	private List<Usuario> listaUsuarioSeleccion;
	private List<Usuario> listaUsuarioModificar;
	
	@PostConstruct
	public void init() {
		this.setUsuario(new Usuario());
		this.listar();
		setAux("F");
	}

	public void listar() {
		try {
			this.setLista(this.usuarioService.listarPorUsuario(getUsuario()));
		} catch (Exception e) {
			
		}
	}
	
	public void ConsultarUsuModificar(){
		try {
			if(getUsuarioSeleccion().getId() != null) {
				String ident = getUsuarioSeleccion().getId().toString();
				int id = Integer.parseInt(ident);
				listaUsuarioSeleccion =usuarioService.listarPorIdent(usuarioSeleccion);
				System.out.println("id "+id+" lista usuario "+listaUsuarioSeleccion.get(1));
				setUsuario(getUsuarioSeleccion());
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void verificarPw() {
		String clave = getPassword1();
		
		if(BCrypt.checkpw(clave, getListaUsuarioSeleccion().get(0).getContrasena())) {
			setAux("V");
		}
	}
	
	public void LimpiarCampos() {
		try {
			usuarioSeleccion = new Usuario();
			setPassword1(null);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void modificarPw(){
		String redireccion = null;
		try {
			String clave = this.usuario.getContrasena();
			String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt());
			this.usuario.setContrasena(claveHash);
			this.usuario.setEstado(getListaUsuarioSeleccion().get(0).getEstado());
			this.usuario.setId(getListaUsuarioSeleccion().get(0).getId());
			this.usuario.setUsuario(getListaUsuarioSeleccion().get(0).getUsuario());
			this.usuarioService.modificar(usuario);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public List<Usuario> getLista() {
		return lista;
	}

	public void setLista(List<Usuario> lista) {
		this.lista = lista;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuarioSeleccion() {
		return usuarioSeleccion;
	}

	public void setUsuarioSeleccion(Usuario usuarioSeleccion) {
		this.usuarioSeleccion = usuarioSeleccion;
	}

	public List<Usuario> getListaUsuarioSeleccion() {
		return listaUsuarioSeleccion;
	}

	public void setListaUsuarioSeleccion(List<Usuario> listaUsuarioSeleccion) {
		this.listaUsuarioSeleccion = listaUsuarioSeleccion;
	}

	public List<Usuario> getListaUsuarioModificar() {
		return listaUsuarioModificar;
	}

	public void setListaUsuarioModificar(List<Usuario> listaUsuarioModificar) {
		this.listaUsuarioModificar = listaUsuarioModificar;
	}

	public Usuario getContrasenaNew() {
		return contrasenaNew;
	}

	public void setContrasenaNew(Usuario contrasenaNew) {
		this.contrasenaNew = contrasenaNew;
	}

	public String getPassword1() {
		return Password1;
	}

	public void setPassword1(String password1) {
		Password1 = password1;
	}

	public String getAux() {
		return aux;
	}

	public void setAux(String aux) {
		this.aux = aux;
	}

	

}
