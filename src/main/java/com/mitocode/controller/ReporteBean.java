package com.mitocode.controller;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.PieChartModel;

import com.mitocode.service.ISeguidorService;
import com.mitocode.util.ReporteSeguidor;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Named
@ViewScoped
public class ReporteBean implements Serializable {

	@Inject
	private ISeguidorService seguidorservice;

	private List<ReporteSeguidor> lista;
	private PieChartModel pieModel1;
	private LineChartModel lineModel1;

	@PostConstruct
	public void init() {
		this.listarSeguidores();
		this.createPieModel1();
		this.createLineModel1();
	}

	private void createLineModel1() {
		lineModel1 = new LineChartModel();

		for (ReporteSeguidor x : this.lista) {
			LineChartSeries series = new LineChartSeries();
			series.setLabel(x.getPublicador());
			series.set(x.getCantidad(), x.getCantidad());
			lineModel1.addSeries(series);
		}

		lineModel1.setTitle("Seguidores - Lineal");
		lineModel1.setLegendPosition("e");
		Axis yAxis = lineModel1.getAxis(AxisType.Y);
		yAxis.setMin(0);
		yAxis.setMax(10);
	}

	private void createPieModel1() {
		pieModel1 = new PieChartModel();

		for (ReporteSeguidor x : this.lista) {
			pieModel1.set(x.getPublicador(), x.getCantidad());
		}

		pieModel1.setTitle("Cantidad de Seguidores");
		pieModel1.setLegendPosition("w");
		pieModel1.setShowDataLabels(true);
	}

	public void listarSeguidores() {
		try {
			lista = seguidorservice.listarSeguidores();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void generarReporte() {

		try {

			Map<String, Object> parametros = new HashMap<String, Object>();
			// parametros.put("", ""); 
			File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/reports/mini-blog.jasper"));
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parametros, new JRBeanCollectionDataSource(this.lista));

			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
			response.addHeader("Content-disposition", "attachment; filename=mini-blog.pdf");
			ServletOutputStream stream = response.getOutputStream();

			JasperExportManager.exportReportToPdfStream(jasperPrint, stream);

			stream.flush();
			stream.close();
			FacesContext.getCurrentInstance().responseComplete();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void verPDF() {
		try {

			File jasper = new File(FacesContext.getCurrentInstance().getExternalContext()
					.getRealPath("/resources/reports/mini-blog.jasper"));

			byte[] bytes = JasperRunManager.runReportToPdf(jasper.getPath(), null,
					new JRBeanCollectionDataSource(this.lista));
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);

			ServletOutputStream outStream = response.getOutputStream();
			outStream.write(bytes, 0, bytes.length);
			outStream.flush();
			outStream.close();

			FacesContext.getCurrentInstance().responseComplete();

		} catch (Exception e) {

		}
	}

	public List<ReporteSeguidor> getLista() {
		return lista;
	}

	public void setLista(List<ReporteSeguidor> lista) {
		this.lista = lista;
	}

	public PieChartModel getPieModel1() {
		return pieModel1;
	}

	public void setPieModel1(PieChartModel pieModel1) {
		this.pieModel1 = pieModel1;
	}

	public LineChartModel getLineModel1() {
		return lineModel1;
	}

	public void setLineModel1(LineChartModel lineModel1) {
		this.lineModel1 = lineModel1;
	}

}
