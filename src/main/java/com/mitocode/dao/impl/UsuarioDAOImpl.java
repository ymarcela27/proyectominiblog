package com.mitocode.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.mitocode.dao.IUsuarioDAO;
import com.mitocode.model.Persona;
import com.mitocode.model.Usuario;

@Stateless
public class UsuarioDAOImpl  implements IUsuarioDAO, Serializable {

	@PersistenceContext(unitName = "blogPU")
	private EntityManager em;

	@Override
	public Integer registrar(Usuario us) throws Exception {
		em.persist(us);
		return us.getPersona().getIdPersona();
	}

	@Override
	public Integer modificar(Usuario us) throws Exception {
		em.merge(us);
		if (us.getContrasena() != null) {
			Query query = em.createQuery("UPDATE Usuario SET contrasena = ?1 WHERE id = ?2");
			query.setParameter(1, us.getContrasena());
			query.setParameter(2, us.getId());
			query.executeUpdate();
		}
		return us.getId();
	}

	@Override
	public List<Usuario> listar() throws Exception {
		List<Usuario> lista = new ArrayList<Usuario>();

		try {
			Query query = em.createQuery("SELECT p FROM Usuario p");
			lista = (List<Usuario>) query.getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}

	@Override
	public Usuario listarPorId(Usuario Usuario) throws Exception {
		Usuario us = new Usuario();
		List<Usuario> lista = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM Usuario u where u.id =?1");
			query.setParameter(1, Usuario.getPersona().getIdPersona());

			lista = (List<Usuario>) query.getResultList();

		} catch (Exception e) {
			throw e;
		}
		return us;
	}

	@Override
	public String traerPassHashed(String us) {
		Usuario usuario = null;
		String consulta;
		try {
			consulta = "FROM Usuario u WHERE u.usuario = ?1";
			Query query = em.createQuery(consulta);
			query.setParameter(1, us);

			List<Usuario> lista = query.getResultList();
			if (!lista.isEmpty()) {
				usuario = lista.get(0);
			}
		} catch (Exception e) {
			throw e;
		}
		return usuario != null ? usuario.getContrasena() : "";
	}

	@Override
	public Usuario login(Usuario us) {
		Usuario usuario = new Usuario();
		String consulta;
		try {
			consulta = "FROM Usuario us WHERE us.usuario = ?1 and us.contrasena = ?2";
			Query query = em.createQuery(consulta);
			query.setParameter(1, us.getUsuario());
			query.setParameter(2, us.getContrasena());

			List<Usuario> lista = query.getResultList();
			if (!lista.isEmpty()) {
				usuario = lista.get(0);
			}
		} catch (Exception e) {
			throw e;
		}
		return usuario;
	}

	@Override
	public Integer eliminar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Usuario> listarPorUsuario(Usuario usuario) {
		List<Usuario> lista = new ArrayList<Usuario>();
		Usuario user = new Usuario();
		user.setUsuario("%"+usuario.getUsuario()+"%");
		try {			
			Query query = em.createQuery("SELECT us FROM Usuario us WHERE us.usuario like :usuario ").setParameter("usuario", user.getUsuario());
			lista = (List<Usuario>) query.getResultList();
			for (Usuario usuario2 : lista) {
				if(("A").equals(usuario2.getEstado()))
				{
					usuario2.setEstadoDesc("ACTIVO");
				}
					else {
						usuario2.setEstadoDesc("INACTIVO");
				}
			}
		} catch (Exception e) {
			throw e;
		}
		return lista;
	}

	@Override
	public List<Usuario> listarPorIdent(Usuario usuarioSeleccion) {
		List<Usuario> lista = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM Usuario p where p.id =:usuarioSeleccion").setParameter("usuarioSeleccion", usuarioSeleccion.getId());
			lista = (List<Usuario>) query.getResultList();
		} catch (Exception e) {
			throw e;
		}
		
		return lista;
	}

	

}
