package com.mitocode.dao;

import java.util.List;

import javax.ejb.Local;

import com.mitocode.model.Usuario;

@Local
public interface IUsuarioDAO extends ICRUD<Usuario> {

	String traerPassHashed(String us);
	Usuario login(Usuario us);
	List<Usuario> listarPorUsuario(Usuario usuario);
	List<Usuario> listarPorIdent(Usuario usuarioSeleccion);
}
